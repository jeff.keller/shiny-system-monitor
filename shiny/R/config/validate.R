
# Build a config.json validator function using JSON Schema
config_validator <- jsonvalidate::json_validator("schema.json")

config_loader <- function(config_file, config_default) {
  
  levellog(logger, "INFO", "Loading configuration")
  
  # Check whether configuration file is valid JSON
  config_raw <- readLines(config_file, warn = FALSE)
  is_config_valid <- jsonlite::validate(config_raw)
  if (!is_config_valid) {
    levellog(logger, "ERROR", glue("Configuration file is not valid JSON:\n{attr(is_config_valid, 'err')}"))
    levellog(logger, "WARN", "Using default configuration values")
    return(config_default)
  }
  
  # Check configuration file against schema
  valid_if_null <- tryCatch(
    config_validator(config_raw, error = TRUE),
    error = function(e) {
      levellog(logger, "ERROR", glue("Invalid configuration file:\n{e$message}"))
      levellog(logger, "WARN", "Using default configuration values")
      return(FALSE)
    }
  )
  if (!is.null(valid_if_null))
    return(config_default)
  
  # If all tests have passed, parse the config and use it
  return(jsonlite::fromJSON(config_raw))
  
}
