# Shiny System Monitor

# Table of Contents

* [Overview](#overview)
* [Server Installation](#server-installation)
    * [Install R](#install-r)
    * [Install Dependencies](#install-dependencies)
    * [Starting the Server](#starting-the-server)
    * [Configuration](#configuration)
        * [`start`](#start)
        * [`config.json`](#config.json)
* [Configure Clients](#configure-clients)
    * [Android](#android)
    * [iOS](#ios)
    * [Raspberry Pi](#raspberry-pi)
* [TODO List](#todo-list)

# Overview

Shiny System Monitor is a web-based computer resource monitor written in R.
It exposes a [`shiny`](https://shiny.rstudio.com/) web server that can be viewed in just about any browser client, though it is primarily designed for small screen devices (think phone or tablet size).
Even very low power devices like the Raspberry Pi Zero W can be used as monitor clients.

Shiny System Monitor runs on a server&mdash;the computer to be monitored.
Multiple browser clients can then connect and view the monitor.
Even a device mounted in or on the computer case!

<img src="img/shiny_system_monitor.jpg" style="max-height: 400px;">

# Server Installation

## Requirements

Shiny System Monitor only supports Linux with Nvidia GPUs* on the monitored PC (the server).

It also requires existing installations of:

* `nvidia-smi` (from Nvidia proprietary drivers)
* `top`
* `free`
* `lm-sensors` (be sure to run `sensors-detect` too)
* `R` (available in most package managers)

_* Support for other operating systems and hardware is on the [TODO List](#todo-list) and will be added if there is enough interest._

## Install R

R is a statistical language that has been overshadowed by the rise of Python in recent years, but has a rich history in data analysis and visualization.

### Debian/Ubuntu Derivatives

```bash
apt install r-base r-base-dev
```

### Other Linux

TODO

## Install Dependencies

```bash
git clone https://gitlab.com/jeff.keller/shiny-system-monitor.git
```

R package dependencies are checked every time Shiny System Monitor starts.
Missing packages are installed automatically.
The very first time you run the monitor you may be asked to give [`renv`](https://rstudio.github.io/renv/articles/renv.html) (an R package manager) consent to install packages.

## Starting the Server

Shiny System Monitor can be started by running the `start` file.

```bash
cd shiny-system-monitor
./start
```

Open a browser window on the server and navigate to [http://127.0.0.1:7979](http://127.0.0.1:7979) to test whether the monitor is working.

## Configuration

### `start`

There are a few server options that can be modified in the `start` file, such as the host and port for the web server.
For example, change the host from `"127.0.0.1"` to `"0.0.0.0"` if you plan to use another device to view the monitor.

You may also want to give the monitored PC a static IP address in your LAN router.
This will make it easier to connect clients without needing to reconfigure them every time the IP address of the monitored PC changes.

### `config.json`

The look and behavior of the monitor can be customized with `config.json`.
It will be reloaded automatically when changes to this file are detected.

| Parameter                        | Default       | Description                                                                             |
|:---------------------------------|:--------------|:----------------------------------------------------------------------------------------|
| _plotly.background_color_        | `"white"`     | Background color of the main chart area.                                                |
| _plotly.gauge.title_color_       | `"dimgray"`   | Color of the gauge chart titles.                                                        |
| _plotly.gauge.bar_color_         | `"dimgray"`   | Color of the gauge bar.                                                                 |
| _plotly.gauge.background_color_  | `"lightgray"` | Color of the gauge bar background (the unfilled portion).                               |
| _plotly.gauge.border_color_      | `"white"`     | Color of the gauge bar border.                                                          |
| _plotly.gauge.value_color_       | `"darkgray"`  | Color of the gauge value text.                                                          |
| _disconnected.show_notification_ | `false`       | Whether to show a message when a client is attempting to reconnect to the monitored PC. |
| _disconnected.background_color_  | `"black"`     | Background color of the client screen when disconnected from the monitored PC.          |

> **Note:** Color parameters can be set to any valid CSS color string (e.g., `"red"` or `"#FF0000"`).

#### A dark theme

```json
{
  "plotly": {
    "background_color": "black",
    "gauge": {
      "title_color": "white",
      "bar_color": "white",
      "background_color": "#333333",
      "border_color": "black",
      "value_color": "white"
    }
  }
}
```

<img src="img/plotly_config_dark.png" style="max-height: 400px;">

#### A light theme

```json
{
  "plotly": {
    "background_color": "white",
    "gauge": {
      "title_color": "dimgray",
      "bar_color": "dimgray",
      "background_color": "lightgray",
      "border_color": "white",
      "value_color": "darkgray"
    }
  }
}
```

<img src="img/plotly_config_light.png" style="max-height: 400px; border: 1px solid lightgray">

# Configure Clients

Just about any device with a browser can act as a Shiny System Monitor client&mdash;phones, tablets, and Raspberry Pis to name a few.
An old phone could be given new life as a Shiny System Monitor client.

Configuring a client boils down to navigating a web browser on the client device to the IP address of the monitored PC and the `port` specified in the server [`start`](#start) file.
For example: http://192.168.1.166:7979.

Additional configurations might be to ensure the browser opens in fullscreen mode and that the client device doesn't go to sleep.

The sections below cover some of the extra device-specific steps you might want to take to automate your clients, such as:

* Reconnecting to the monitored PC when the _client device_ reboots
* Hiding the mouse pointer on desktop client devices (e.g., Raspberry Pi)

> **Note:** Clients will automatically try to reconnect to the monitored PC every 10 seconds if the connection is lost (e.g., monitored PC reboots, network disruption).

## Android

TODO

## iOS

TODO

## Raspberry Pi

A Raspberry Pi makes a good monitor client when paired with a small display. TODO: LINK

### Hardware

The following Raspberry Pi models have been tested and confirmed to work well.

* Model 3 A+
* Model 3 B
* Zero W*

_* The Raspberry Pi Zero W has limited computing power which results in choppy update animations._

### Autostart

1. Install the Chromium browser. If you are using Raspbian with your Raspberry Pi, this is probably already installed.

    ```bash
    sudo apt install chromium-browser
    ```

2. Install `unclutter` so the mouse pointer can be hidden.

    ```bash
    sudo apt install unclutter
    ```

3. Clone this repository.

    ```bash
    git clone https://gitlab.com/jeff.keller/shiny-system-monitor.git
    ```

4. Edit the IP address and port in the `shiny-system-monitor/client/rpi` file as necessary. The port should match the port you set in the server `start` file.

    Test that everything works by running the file manually:
    
    ```bash
    ./shiny-system-monitor/client/rpi
    ```

5. To ensure the `rpi` file runs on startup, add a line to the LXDE autostart file (`/etc/xdg/lxsession/LXDE-pi/autostart`):

    ```bash
    @/home/pi/shiny-system-monitor/client/rpi
    ```

### Other

Some other tweaks you might want to make to your Raspberry Pi client setup:

* [Rotate the display](https://www.raspberrypi-spy.co.uk/2017/11/how-to-rotate-the-raspberry-pi-display-output/)
* [Adjust display brightness](https://raspberrypi.stackexchange.com/questions/46225/adjusting-the-brightness-of-the-official-touchscreen-display)
* [Disable swap](https://www.raspberrypi.org/forums/viewtopic.php?p=1488821#p1520314) (to avoid excess writes to the SD card)
* System Configuration Settings GUI
  * Wait for a network connection before user login
  * Disable screen blanking (screen sleep)

# TODO List

* Support for AMD GPUs
* Support for Windows
* Support for MacOS
* Build [plotly](https://plotly.com/) plug-in framework to allow custom monitors
* Support for custom layouts beyond a 3x2 grid
